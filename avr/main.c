#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <stdio.h>
#include "io.h"
#include "uart.h"
//#include <avr/pgmspace.h>
//#include <avr/eeprom.h>

#define TRIAC_1 D,5
#define TRIAC_2 D,6
#define ZC D,3

//Interrupt Service Routine for INT0
volatile uint16_t dimming = 90;

ISR(INT1_vect){
  //static uint16_t dimtime = 9000;// (100*dimming);
  //static uint16_t dimtime = 9000; //(100*dimming);
  uint16_t dimtime = dimming;
  while(dimtime--) _delay_us(100);
  //_delay_us(9000);

  GPIO_HIGH(TRIAC_1);  
  _delay_us(10);
  GPIO_LOW(TRIAC_1);
}

int main(void){
  PRINTF_TO_UART;
  int i=0;
  GPIO_OUTPUT(TRIAC_1);
  GPIO_OUTPUT(TRIAC_2);
  GPIO_INPUT(ZC);

  GPIO_LOW(TRIAC_1);
  GPIO_LOW(TRIAC_2);

  GICR = (1<<INT1); // Enable INT1  
  MCUCR = (1<<ISC10); // Trigger INT1 on  any edge

  uart_init();
  printf("Hello from Atmega8 side.");
  sei();    

  while(1){
    uint8_t on = 90;
    uint8_t off = 6;  
    for (i=off; i <= on; i++){
      dimming=i;
      printf("Dimming + is: %d\n", dimming);      
      _delay_ms(10);
    }
    printf("Next turn\n");
    for (i=on; i >= off; i--){
      dimming=i;
      printf("Dimming - is: %d\n", dimming);      
      _delay_ms(10);
    }

  }
  return 0;
}
