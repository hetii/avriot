/*
 * Examples:
 *
 * #define LED_DIR BITP(DDRB, PB4); // LED_DIR is "variable" pointing to single bit (PB4) in DDRB
 * #define LED_STATE BITP(PORTB, PB4); // LED_STATE points to single bit (PB4) in PORTB
 * LED_DIR = OUT; // Set pin as output
 * LED_STATE = ON; // Switch LED on
 * 
 * set(DDRB, PB3, PB5, PB7); // Set bits PB3, PB5 and PB7 in DDRB, leave other bits untouched.
 * set_ex(DDRB, PB7); // Sets only PB7 in DDRB, other bits are zeroed (set exclusive).
 * clr(PORTB, PB4); // Clear bit PB4 in PORTB, leave other bits untouched.
 */

#ifndef _DRVPACK_IO_H_
#define _DRVPACK_IO_H_ 1

#include <avr/io.h>
#include <util/delay.h>

// Struct to handle bits in registers.
typedef struct {
	uint8_t b0:1;
	uint8_t b1:1;
	uint8_t b2:1;
	uint8_t b3:1;
	uint8_t b4:1;
	uint8_t b5:1;
	uint8_t b6:1;
	uint8_t b7:1;
} bitmask_s;

// Helper macros for bit pointer macro.
#define BNAME(x) BSTR(x)
#define BSTR(x) b##x

// Common defines.
#define OUT 1
#define IN 0
#define ON 1
#define OFF 0

// Concatenate helper.
#define IO_CONCAT(a, ...) IO_CONCAT_IMPL(a, __VA_ARGS__)
#define IO_CONCAT_IMPL(x, ...) x ## __VA_ARGS__

#define xIO_CONCAT(...) IO_CONCAT_IMPL( __VA_ARGS__)
#define xIO_CONCAT_IMPL(x, ...) x ## __VA_ARGS__

// Count number of arguments in __VA_ARGS__.
#define VA_NUM_ARGS_IMPL(_1, _2, _3, _4, _5, _6, _7, _8, x, ...) x
#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL(__VA_ARGS__, 8, 7, 6, 5, 4, 3, 2, 1)

// Bit-Shift-Or macros for 1-8 parameters. 
#define BSO_1(a) (1 << a)
#define BSO_2(a, b) (1 << a) | (1 << b)
#define BSO_3(a, b, c) (1 << a) | (1 << b) | (1 << c)
#define BSO_4(a, b, c, d) (1 << a) | (1 << b) | (1 << c) | (1 << d)
#define BSO_5(a, b, c, d, e) (1 << a) | (1 << b) | (1 << c) | (1 << d) | (1 << e)
#define BSO_6(a, b, c, d, e, f) (1 << a) | (1 << b) | (1 << c) | (1 << d) | (1 << e) | (1 << f)
#define BSO_7(a, b, c, d, e, f, g) (1 << a) | (1 << b) | (1 << c) | (1 << d) | (1 << e) | (1 << f) | (1 << g)
#define BSO_8(a, b, c, d, e, f, g, h) (1 << a) | (1 << b) | (1 << c) | (1 << d) | (1 << e) | (1 << f) | (1 << g) | (1 << h)

// Bit pointer macro which can be used to make individual bit variables.
#define BITP(var, bit) ((volatile bitmask_s*)&var)->BNAME(bit)

// Macros to set and clear multiple bits in registers.
#define SET(x, ...) x |= (IO_CONCAT(BSO_, VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))
#define CLR(x, ...) x &= ~(IO_CONCAT(BSO_, VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))
#define SET_EX(x, ...) x = (IO_CONCAT(BSO_, VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))
#define TOOGLE(x, ...) x ^= (IO_CONCAT(BSO_, VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))

// Return true if any bit is set.
#define GET(p, ...) (((p) & (IO_CONCAT(BSO_, VA_NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))) != 0)

// Macros to use shortcut name like: B,4.
#define _SET(type, name, ...) SET(type ## name, __VA_ARGS__) 
#define _CLR(type, name, ...) CLR(type ## name, __VA_ARGS__)
#define _GET(type, name, ...) GET(type ## name, __VA_ARGS__)
#define _TOGGLE(type, name, ...) TOOGLE(type ## name, __VA_ARGS__)  

#define GPIO_INPUT(pin)  _CLR(DDR,  pin)
#define GPIO_OUTPUT(pin) _SET(DDR,  pin)
#define GPIO_HIGH(pin)   _SET(PORT, pin)
#define GPIO_LOW(pin)    _CLR(PORT, pin)
#define GPIO_PULLUP(pin) _SET(PORT, pin)
#define GPIO_IS_HIGH(pin) _GET(PIN,  pin)
#define GPIO_TOGGLE(pin) _TOGGLE(PORT, pin)

#endif
