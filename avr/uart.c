#include <avr/interrupt.h>
#include <stddef.h>
#include "uart.h"

static volatile uint8_t i = 0;
static volatile uint8_t is_rx_ready = 0;
static char uart_rx[250];
static char uart_tx[250];

ISR(USART_RX_vect){
  static char uart_char;
  uart_char = UART_UDR;

  if (i > sizeof(uart_rx)){
    i = 0;
  } else if (uart_char == '\n'){
    uart_rx[i++] = '\n';
    is_rx_ready = 1;
    i = 0;
  } else {
    uart_rx[i++] = uart_char;
  }
}

char *get_uart_rx(void){
  if (is_rx_ready){
    is_rx_ready = 0;
    return uart_rx;
  }
  return NULL;
}

#ifdef SOFT_UART
  void uart_delay() {
    __asm__ __volatile__ (
      "ldi r25,%[count]\n"
      "1:dec r25\n"
      "brne 1b\n"
      "ret\n"
      ::[count] "M" (UART_B_VALUE)
    );
  }
#endif

void uart_init(){
  #ifdef SOFT_UART
    // Set TX pin as output.
    UART_DDR |= _BV(UART_TX_BIT);
  #else
    #if defined(__AVR_ATmega8__) || defined (__AVR_ATmega32__) || defined (__AVR_ATmega16__)
      UCSRA = _BV(U2X); //Double speed mode USART
      UCSRB = _BV(RXEN) | _BV(TXEN);  // enable Rx & Tx
      UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);  // config USART; 8N1
      UBRRL = (uint8_t)( (F_CPU + BAUD_RATE * 4L) / (BAUD_RATE * 8L) - 1 );
      #ifdef USE_INTERRUPT
        UCSRB |= (1 << RXCIE); // Enable the USART Recieve Complete interrupt (USART_RXC)
      #endif
    #else
      UART_SRA = _BV(U2X0); //Double speed mode USART0
      UART_SRB = _BV(RXEN0) | _BV(TXEN0);
      UART_SRC = _BV(UCSZ00) | _BV(UCSZ01);
      UART_SRL = (uint8_t)( (F_CPU + BAUD_RATE * 4L) / (BAUD_RATE * 8L) - 1 );
      #ifdef USE_INTERRUPT
        UART_SRB |= (1 << RXCIE0); // Enable the USART Recieve Complete interrupt (USART_RXC)
      #endif
    #endif
  #endif
  #ifdef DEBUG_ENABLE
    printf("Uart initialized.\n");
  #endif
}

uint8_t uart_getc(void) {
  uint8_t ch;
  #ifdef SOFT_UART
     // watchdogReset();
    __asm__ __volatile__ (
      "1: sbic  %[uartPin],%[uartBit]\n" // Wait for start edge
      "   rjmp  1b\n"
      "   rcall uart_delay\n"            // Get to middle of start bit
       "2: rcall uart_delay\n"            // Wait 1 bit period
      "   rcall uart_delay\n"            // Wait 1 bit period
      "   clc\n"
      "   sbic  %[uartPin],%[uartBit]\n"
      "   sec\n"
      "   dec   %[bitCnt]\n"
      "   breq  3f\n"
      "   ror   %[ch]\n"
      "   rjmp  2b\n"
      "3:\n"
      :
        [ch] "=r" (ch)
      :
        [bitCnt] "d" (9),
        [uartPin] "I" (_SFR_IO_ADDR(UART_PIN)),
        [uartBit] "I" (UART_RX_BIT)
      :
        "r25"
    );
  #elif defined(USE_INTERRUPT)
    return 0;
  #else
    while(!(UART_SRA & _BV(RXC0)));
    //if (!(UART_SRA & _BV(FE0))) {
        /*
         * A Framing Error indicates (probably) that something is talking
         * to us at the wrong bit rate.  Assume that this is because it
         * expects to be talking to the application, and DON'T reset the
         * watchdog.  This should cause the bootloader to abort and run
         * the application "soon", if it keeps happening.  (Note that we
         * don't care that an invalid char is returned...)
        */
      //watchdogReset();
    //}
    ch = UART_UDR;
  #endif
  return ch;
}

void uart_putc(char ch) {
  #ifndef SOFT_UART
    while (!(UART_SRA & _BV(UDRE0)));
    UART_UDR = ch;
  #else
    __asm__ __volatile__ (
      "   com %[ch]\n" // ones complement, carry set
      "   sec\n"
      "1: brcc 2f\n"
      "   cbi %[uartPort],%[uartBit]\n"
      "   rjmp 3f\n"
      "2: sbi %[uartPort],%[uartBit]\n"
      "   nop\n"
      "3: rcall uart_delay\n"
      "   rcall uart_delay\n"
      "   lsr %[ch]\n"
      "   dec %[bitcnt]\n"
      "   brne 1b\n"
      :
      :
        [bitcnt] "d" (10),
        [ch] "r" (ch),
        [uartPort] "I" (_SFR_IO_ADDR(UART_PORT)),
        [uartBit] "I" (UART_TX_BIT)
      :
        "r25"
    );
  #endif
}

void uart_puts(char *string){
    while(*string){
        uart_putc(*string++);
    }
}

void uart_puts_P(PGM_P s){
    uint8_t ch;
    while ((ch = pgm_read_byte(s)) != '\0'){
        uart_putc(ch);
        s++;
    }
}

#ifdef DEBUG_ENABLE
  // this function is called by printf as a stream handler
  static int uart_putc_printf(char var, FILE *stream){
      // translate \n to \r for br@y++ terminal
      if (var == '\n') uart_putc('\r');
      uart_putc(var);
      return 0;
  }
  FILE printf_to_uart = FDEV_SETUP_STREAM(uart_putc_printf, NULL, _FDEV_SETUP_WRITE);
#endif
