#ifndef _DRVPACK_UART_H_
#define _DRVPACK_UART_H_ 1

#include <inttypes.h>
#include <avr/pgmspace.h>
#include <avr/io.h>

//#define SOFT_UART 1
//#define BAUD_RATE 2000000L
#define DEBUG_ENABLE 1

#ifndef BAUD_RATE
  #define BAUD_RATE 115200L
#endif

#define USE_INTERRUPT 1

/*------------------------------------------------\
| Real test result:         |   ATMEGA8@16MHz     |
|---------------------------|---------------------|
|                           | SOFTWARE | HARDWARE |
|#define BAUD_RATE 300L     | too slow | too slow |
|#define BAUD_RATE 1200L    | too slow | too slow |
|#define BAUD_RATE 2400L    | too slow | too slow |
|#define BAUD_RATE 4800L    | too slow | too slow |
|#define BAUD_RATE 9600L    | too slow | ok       |
|#define BAUD_RATE 19200L   | ok       | ok       |
|#define BAUD_RATE 38400L   | ok       | ok       |
|#define BAUD_RATE 57600L   | ok       | ok       |
|#define BAUD_RATE 115200L  | err > 2% | err > 2% |
|#define BAUD_RATE 230400L  | err > 2% | err > 2% |
|#define BAUD_RATE 460800L  | err > 5% | err > 5% |
|#define BAUD_RATE 500000L  | junks    | ok       |
|#define BAUD_RATE 576000L  | junks    | err > 5% |
|#define BAUD_RATE 921600L  | err > 5% | err > 5% |
|#define BAUD_RATE 1000000L | nothing? | ok       |
|#define BAUD_RATE 1152000L | err > 5% | err > 5% |
|#define BAUD_RATE 1500000L | err > 5% | err > 5% |
|#define BAUD_RATE 2000000L | asm err  | ok       |
|#define BAUD_RATE 2500000L | err > 5% | err > 5% |
|#define BAUD_RATE 3000000L | err > 5% | err > 5% |
|#define BAUD_RATE 3500000L | err > 5% | err > 5% |
|#define BAUD_RATE 4000000L | err > 5% | err > 5% |
\------------------------------------------------*/

// Default configuration:
#ifdef SOFT_UART
  // Ports for soft UART.
  #define UART_PORT   PORTD
  #define UART_PIN    PIND
  #define UART_DDR    DDRD
  #define UART_RX_BIT PD0
  #define UART_TX_BIT PD1
#else
  // Real hardware on uC.
  #ifndef UART
    #define UART 0
  #endif
#endif

// Handle devices with up to 4 uarts (eg m1280.) Rather inelegantly.
// Note that mega8/m32 still needs special handling, because ubrr is handled
// differently.
#if UART == 0
# define UART_SRA UCSR0A
# define UART_SRB UCSR0B
# define UART_SRC UCSR0C
# define UART_SRL UBRR0L
# define UART_UDR UDR0
#elif UART == 1
#if !defined(UDR1)
#error UART == 1, but no UART1 on device
#endif
# define UART_SRA UCSR1A
# define UART_SRB UCSR1B
# define UART_SRC UCSR1C
# define UART_SRL UBRR1L
# define UART_UDR UDR1
#elif UART == 2
#if !defined(UDR2)
#error UART == 2, but no UART2 on device
#endif
# define UART_SRA UCSR2A
# define UART_SRB UCSR2B
# define UART_SRC UCSR2C
# define UART_SRL UBRR2L
# define UART_UDR UDR2
#elif UART == 3
#if !defined(UDR3)
#error UART == 3, but no UART3 on device
#endif
# define UART_SRA UCSR3A
# define UART_SRB UCSR3B
# define UART_SRC UCSR3C
# define UART_SRL UBRR3L
# define UART_UDR UDR3
#endif

#if defined(__AVR_ATmega8__) || defined (__AVR_ATmega32__) || defined (__AVR_ATmega16__)
  //Name conversion R.Wiersma
  #define UCSR0A        UCSRA
  #define UDR0          UDR
  #define UDRE0         UDRE
  #define RXC0          RXC
  #define FE0           FE
  #define TIFR1         TIFR
  #define WDTCSR        WDTCR
  #define USART_RX_vect USART_RXC_vect
#endif
#if defined (__AVR_ATmega32__) || defined (__AVR_ATmega16__)
  #define WDCE          WDTOE
#endif

// Set the UART baud rate defaults.
#ifndef BAUD_RATE
  #if F_CPU >= 8000000L
    // Highest rate Avrdude win32 will support.
    #define BAUD_RATE 115200L
  #elif F_CPU >= 1000000L
    // 19200 also supported, but with significant error.
    #define BAUD_RATE 9600L
  #elif F_CPU >= 128000L
    // Good for 128kHz internal RC.
    #define BAUD_RATE 4800L
  #else
    // Good even at 32768Hz
    #define BAUD_RATE 1200L
  #endif
#endif

#define BAUD_SETTING (( (F_CPU + BAUD_RATE * 4L) / ((BAUD_RATE * 8L))) - 1 )
#define BAUD_ACTUAL (F_CPU/(8 * ((BAUD_SETTING)+1)))

#if BAUD_ACTUAL <= BAUD_RATE
  #define BAUD_ERROR (( 100*(BAUD_RATE - BAUD_ACTUAL) ) / BAUD_RATE)
  #if BAUD_ERROR >= 5
    #error BAUD_RATE error greater than -5%
  #elif BAUD_ERROR >= 2
    #warning BAUD_RATE error greater than -2%
  #endif
#else
  #define BAUD_ERROR (( 100*(BAUD_ACTUAL - BAUD_RATE) ) / BAUD_RATE)
  #if BAUD_ERROR >= 5
    #error BAUD_RATE error greater than 5%
  #elif BAUD_ERROR >= 2
    #warning BAUD_RATE error greater than 2%
  #endif
#endif

// Baud rate slow check.
#if (F_CPU + BAUD_RATE * 4L) / (BAUD_RATE * 8L) - 1 > 250
  #error Unachievable baud rate (too slow) BAUD_RATE 
#endif

// Baud rate fastn check.
#if (F_CPU + BAUD_RATE * 4L) / (BAUD_RATE * 8L) - 1 < 3
  // Permit high bitrates (ie 1Mbps@16MHz) if error is zero.
  #if BAUD_ERROR != 0
    #error Unachievable baud rate (too fast) BAUD_RATE 
  #endif
#endif

#ifdef SOFT_UART
  // AVR305 equation: #define UART_B_VALUE (((F_CPU/BAUD_RATE)-23)/6)
  // Adding 3 to numerator simulates nearest rounding for more accurate baud rates
  #define UART_B_VALUE (((F_CPU/BAUD_RATE)-20)/6)
  #if UART_B_VALUE > 255
    #error Baud rate too slow for soft UART
  #endif
#endif

void uart_init(void);
uint8_t uart_getc(void);
void uart_putc(char ch);
void uart_puts(char *text);
void uart_puts_P(PGM_P s);
char *get_uart_rx(void);

#ifdef DEBUG_ENABLE
  #include <stdio.h>
  extern FILE printf_to_uart;
  #define PRINTF_TO_UART stdout = &printf_to_uart
#else
  #define PRINTF_TO_UART
#endif

#endif
